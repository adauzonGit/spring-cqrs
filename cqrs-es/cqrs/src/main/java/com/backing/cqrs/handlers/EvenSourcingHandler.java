package com.backing.cqrs.handlers;

import com.backing.cqrs.domain.AggregateRoot;

public interface EvenSourcingHandler<T> {

    void save(AggregateRoot root);
    T getById(String id);
}
