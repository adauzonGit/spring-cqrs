package com.backing.account.cmd.api.controllers;

import com.backing.account.cmd.api.command.DepositAccountCommand;
import com.backing.account.common.dto.BaseResponse;
import com.backing.account.common.events.FundsDepositedEvent;
import com.backing.cqrs.exceptions.AggregateNotFoundException;
import com.backing.cqrs.infratrutucre.CommandDispacher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/depositFunds")
public class DepositFundsController {
    
    
    private static final Logger log = LoggerFactory.getLogger(DepositFundsController.class);

    @Autowired
    private CommandDispacher commandDispacher;

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse>  depositFunds(@PathVariable(value = "id") String id , @RequestBody DepositAccountCommand fundsDepositedEvent){

        try{
            fundsDepositedEvent.setId(id);
            commandDispacher.send(fundsDepositedEvent);
            return ResponseEntity.status(200).body(new BaseResponse("Operacion exitosa"));

        }catch (IllegalStateException| AggregateNotFoundException ex){
            log.warn("Error al depositar el request {0} ");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponse(ex.toString()));
        } catch (Exception ex){
            log.error("Error al depositar el request  " + id);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new BaseResponse(ex.toString()));
        }

    }
    
}
