package com.backing.account.query.infrastructure.consumers;

import com.backing.account.common.events.AccountClosedEvent;
import com.backing.account.common.events.AccountOpenedEvent;
import com.backing.account.common.events.FundWithDrawEvent;
import com.backing.account.common.events.FundsDepositedEvent;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Payload;

public interface EventConsumer {

    void consumer(@Payload AccountOpenedEvent accountOpenedEvent, Acknowledgment acknowledgment);

    void consumer(@Payload FundWithDrawEvent fundWithDrawEvent, Acknowledgment acknowledgment);

    void consumer(@Payload FundsDepositedEvent fundsDepositedEvent, Acknowledgment acknowledgment);

    void consumer(@Payload AccountClosedEvent accountClosedEvent, Acknowledgment acknowledgment);


}
