package com.backing.account.cmd.infrastructure;

import com.backing.account.cmd.domain.AccountAggregate;
import com.backing.cqrs.domain.AggregateRoot;
import com.backing.cqrs.domain.EventStore;
import com.backing.cqrs.events.BaseEvent;
import com.backing.cqrs.handlers.EvenSourcingHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class AccountEventSourcingHandler implements EvenSourcingHandler<AccountAggregate> {

    @Autowired
    private EventStore eventStore;


    @Override
    public void save(AggregateRoot root) {
        eventStore.saveEvents(root.getId(),root.getUncommitedChanges(),root.getVersion());
        root.markChangesAsCommited();
    }

    @Override
    public AccountAggregate getById(String id) {
        var aggregate = new AccountAggregate();
        List<BaseEvent> event = eventStore.getEvent(id);
        if(event!=null  && !event.isEmpty()){
            aggregate.replayEvents(event);
            Optional<Integer> max = event.stream().map(m -> m.getVersion()).max(Comparator.naturalOrder());

            aggregate.setVersion(max.get());
        }
        return aggregate;
    }
}
