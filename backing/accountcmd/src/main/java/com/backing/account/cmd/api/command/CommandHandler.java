package com.backing.account.cmd.api.command;

public interface CommandHandler {

    void handle(OpenAccountCommand openAccountCommand);

    void handle(DepositAccountCommand depositAccountCommand);

    void handle(WithdrawFundsCommand withdrawFundsCommand);

    void handle(CloseAccountCommand closeAccountCommand);


}
