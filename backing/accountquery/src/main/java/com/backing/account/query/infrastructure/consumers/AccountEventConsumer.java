package com.backing.account.query.infrastructure.consumers;

import com.backing.account.common.events.AccountClosedEvent;
import com.backing.account.common.events.AccountOpenedEvent;
import com.backing.account.common.events.FundWithDrawEvent;
import com.backing.account.common.events.FundsDepositedEvent;
import com.backing.account.query.infrastructure.handlers.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class AccountEventConsumer implements  EventConsumer {



    @Autowired
    private EventHandler eventHandler;


    @KafkaListener(topics = "AccountOpenedEvent", groupId = "${spring.kafka.consumer.group-id}")
    @Override
    public void consumer(@Payload AccountOpenedEvent accountOpenedEvent, Acknowledgment acknowledgment) {
        eventHandler.on(accountOpenedEvent);
        acknowledgment.acknowledge();
    }

    @KafkaListener(topics = {"FundWithDrawEvent"}, groupId = "${spring.kafka.consumer.group-id}")
    @Override
    public void consumer(@Payload FundWithDrawEvent fundWithDrawEvent, Acknowledgment acknowledgment) {
        eventHandler.on(fundWithDrawEvent);
        acknowledgment.acknowledge();
    }

    @KafkaListener(topics = {"FundsDepositedEvent"}, groupId = "${spring.kafka.consumer.group-id}")
    @Override
    public void consumer(@Payload FundsDepositedEvent fundsDepositedEvent, Acknowledgment acknowledgment) {
        eventHandler.on(fundsDepositedEvent);
        acknowledgment.acknowledge();
    }

    @KafkaListener(topics = {"AccountClosedEvent"}, groupId = "${spring.kafka.consumer.group-id}")
    @Override
    public void consumer(@Payload AccountClosedEvent accountClosedEvent, Acknowledgment acknowledgment) {
        eventHandler.on(accountClosedEvent);
        acknowledgment.acknowledge();
    }
}
