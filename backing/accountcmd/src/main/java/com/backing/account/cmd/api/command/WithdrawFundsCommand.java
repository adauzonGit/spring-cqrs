package com.backing.account.cmd.api.command;

import com.backing.cqrs.commands.BaseCommand;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawFundsCommand extends BaseCommand {

    private double ammount;


}
