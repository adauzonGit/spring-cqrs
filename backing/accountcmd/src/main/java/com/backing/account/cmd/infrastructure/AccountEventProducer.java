package com.backing.account.cmd.infrastructure;

import com.backing.cqrs.events.BaseEvent;
import com.backing.cqrs.producer.EventProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.w3c.dom.events.Event;

@Service
public class AccountEventProducer implements EventProducer {

    @Autowired
    private KafkaTemplate<String,Object> kafkaTemplate;

    @Override
    public void produce(String topic, BaseEvent baseEvent) {

        kafkaTemplate.send(topic,baseEvent);
    }
}
