package com.backing.account.query.domain;

import com.backing.cqrs.domain.BaseEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends CrudRepository<BanckAccount,String> {

    Optional<BanckAccount> findByAccountHolder(String accountHolder);

    List<BanckAccount> findByBalanceGreaterThan(double balance);

    List<BaseEntity> findByBalanceLessThan(double balance);
}
