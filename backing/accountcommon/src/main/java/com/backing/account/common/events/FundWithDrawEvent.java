package com.backing.account.common.events;

import com.backing.cqrs.events.BaseEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class FundWithDrawEvent extends BaseEvent {

    private double ammount;
}
