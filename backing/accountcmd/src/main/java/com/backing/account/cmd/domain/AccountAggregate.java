package com.backing.account.cmd.domain;

import com.backing.account.cmd.api.command.OpenAccountCommand;
import com.backing.account.cmd.api.command.WithdrawFundsCommand;
import com.backing.account.common.events.AccountClosedEvent;
import com.backing.account.common.events.AccountOpenedEvent;
import com.backing.account.common.events.FundWithDrawEvent;
import com.backing.account.common.events.FundsDepositedEvent;
import com.backing.cqrs.domain.AggregateRoot;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
public class AccountAggregate extends AggregateRoot {

    private boolean active;
    @Getter
    private double balance;

    public AccountAggregate(OpenAccountCommand command) {
        raisedEvent(AccountOpenedEvent.builder()
                .id(command.getId())
                .accountHolder(command.getAccountHolder())
                .createdDate(new Date())
                .accountType(command.getAccountType())
                .openingBalance(command.getOpeningBalance())
                .build());

    }

    public void apply(AccountOpenedEvent event){
        this.id = event.getId();
        this.active = true;
        this.balance = event.getOpeningBalance();

    }

    public void depositFunds(double ammount){
        if(!this.active){
            throw new  IllegalStateException("Los fondos no pueden ser depositados en la cuenta ");
        }
        if(ammount <= 0){
            throw new IllegalStateException("El deposito de dinero no puede ser 0 o menor de 0");
        }

        raisedEvent(FundsDepositedEvent.builder()
                .id(this.id)
                .ammount(ammount)
                .build());

    }

    public void apply(FundsDepositedEvent event){
        this.id = event.getId();
        this.balance += event.getAmmount();

    }


    public void withDrawFunds(double ammount){
        if(!this.active){
            throw new  IllegalStateException("Cuenta con estado no valido");
        }

        raisedEvent(FundWithDrawEvent.builder()
                .id(this.id)
                .ammount(ammount)
                .build());

    }
    public void apply(FundWithDrawEvent event){
        this.id = event.getId();
        this.balance -= event.getAmmount();

    }

    public void closeAccount(){
        if(!this.active){
            throw new IllegalStateException("La cuenta de banco esta cerrada");
        }

        raisedEvent(AccountClosedEvent.builder()
                .id(this.id)
                .build());
    }
    public void apply(AccountClosedEvent event){
        this.id = event.getId();
        this.active = false;

    }
}
