package com.backing.account.cmd.api.controllers;

import com.backing.account.cmd.api.command.OpenAccountCommand;
import com.backing.account.cmd.api.dto.OpenAccountResponse;
import com.backing.account.common.dto.BaseResponse;
import com.backing.cqrs.infratrutucre.CommandDispacher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/openBankAccount")
public class OpenAccountController {
    
    
    private static final Logger log = LoggerFactory.getLogger(OpenAccountResponse.class);

    @Autowired
    private CommandDispacher commandDispacher;

    @PostMapping
    public ResponseEntity<BaseResponse> openAccount(@RequestBody OpenAccountCommand openAccountCommand){

        var id = UUID.randomUUID().toString();
        openAccountCommand.setId(id);
        try {
            commandDispacher.send(openAccountCommand);
            return ResponseEntity.status(HttpStatus.CREATED.value()).body(new OpenAccountResponse("La cuenta se a creado", id));
        }catch (IllegalStateException ex){
            log.warn(MessageFormat.format("No se pudo generar la cuenta de banco {0}", ex.toString()));
            return ResponseEntity.badRequest().body(new BaseResponse(ex.toString()));
        }catch (Exception ex){
            var safeErrorMessage = MessageFormat.format("Errores mediante creación {0}", id);
            log.error(safeErrorMessage,ex);
            return ResponseEntity.internalServerError().body(new OpenAccountResponse(ex.toString(),id));

        }
    }




}
