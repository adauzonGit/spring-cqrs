package com.backing.account.cmd.api.command;

import com.backing.cqrs.commands.BaseCommand;
import lombok.Data;

@Data
public class DepositAccountCommand extends BaseCommand {

    private double ammount;

}
