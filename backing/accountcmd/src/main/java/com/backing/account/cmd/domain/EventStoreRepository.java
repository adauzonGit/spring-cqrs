package com.backing.account.cmd.domain;

import com.backing.cqrs.events.EventModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EventStoreRepository extends MongoRepository<EventModel,String> {

    List<EventModel> findByAggregateIdentifier(String identifier);

}
