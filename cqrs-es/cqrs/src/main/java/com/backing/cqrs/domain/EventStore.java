package com.backing.cqrs.domain;

import com.backing.cqrs.events.BaseEvent;

import java.util.List;

public interface EventStore {

    void saveEvents(String aggregateId , Iterable<BaseEvent> events, int expectecVersion);

    List<BaseEvent> getEvent(String aggregateId);

}
