package com.backing.account.query.domain;

import com.backing.account.common.dto.AccountType;
import com.backing.cqrs.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class BanckAccount extends BaseEntity {

    @Id
    private String id;

    private String accountHolder;
    private Date creationDate;

    private AccountType accountType;

    private double balance;


}
