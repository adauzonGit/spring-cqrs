package com.backing.cqrs.domain;

import com.backing.cqrs.events.BaseEvent;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


public abstract class AggregateRoot {

    protected String id;
    @Getter
    @Setter
    private int version = -1;
    private final List<BaseEvent> changes = new ArrayList<>();


    private static final Logger log = LoggerFactory.getLogger(AggregateRoot.class);

    public String getId(){
        return this.id;
    }

    public List<BaseEvent> getUncommitedChanges(){
        return changes;
    }

    public void  markChangesAsCommited(){
        this.changes.clear();
    }

    protected void applyChanges(BaseEvent event, boolean isNew){
        try {
            var method = getClass().getDeclaredMethod("apply", event.getClass());
            method.setAccessible(true);
            method.invoke(this,event);
        }catch (NoSuchMethodException ex){
            log.warn(MessageFormat.format("El method apply no fue encontrado para {0}",event.getClass().getName()));
        }catch (Exception ex){
            log.error("Errores aplicando evento",ex);
        }finally {
            if(isNew){
                changes.add(event);
            }
        }
    }

    public void raisedEvent(BaseEvent event){
        applyChanges(event,true);

    }

    public void replayEvents(Iterable<BaseEvent> events){
        events.forEach(event -> {
            applyChanges(event,false);
        });
    }

    
}
