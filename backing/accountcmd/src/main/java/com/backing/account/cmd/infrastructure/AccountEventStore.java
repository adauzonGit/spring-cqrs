package com.backing.account.cmd.infrastructure;

import com.backing.account.cmd.domain.AccountAggregate;
import com.backing.account.cmd.domain.EventStoreRepository;
import com.backing.cqrs.domain.EventStore;
import com.backing.cqrs.events.BaseEvent;
import com.backing.cqrs.events.EventModel;
import com.backing.cqrs.exceptions.AggregateNotFoundException;
import com.backing.cqrs.exceptions.ConcurrencyException;
import com.backing.cqrs.producer.EventProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountEventStore implements EventStore {


    @Autowired
    private EventProducer eventProducer;

    @Autowired
    private EventStoreRepository eventStoreRepository;

    @Override
    public void saveEvents(String aggregateId, Iterable<BaseEvent> events, int expectecVersion) {

        var eventsStream = eventStoreRepository.findByAggregateIdentifier(aggregateId);
        if(expectecVersion != -1 && eventsStream.get(eventsStream.size() -1 ).getVersion() !=  expectecVersion){
            throw new ConcurrencyException();
        }

        var version = expectecVersion;
        for(var event: events){
            version ++;
            event.setVersion(version);
            var eventModel = EventModel.builder()
                    .baseEvent(event)
                    .aggregateIdentifier(aggregateId)
                    .aggrateType(AccountAggregate.class.getTypeName())
                    .version(version)
                    .eventType(event.getClass().getTypeName())
                    .build();
            EventModel save = eventStoreRepository.save(eventModel);

            if(!save.getId().isEmpty()){
                //Producir evento kafka
                eventProducer.produce(event.getClass().getSimpleName(), eventModel.getBaseEvent());
            }
        }

    }

    @Override
    public List<BaseEvent> getEvent(String aggregateId) {
        var eventStream = eventStoreRepository.findByAggregateIdentifier(aggregateId);
        if(eventStream == null || eventStream.isEmpty()){
            throw new AggregateNotFoundException("La cuenta de banco es incorrepta");
        }
        return eventStream.stream().map(x-> x.getBaseEvent()).collect(Collectors.toList());
    }
}
