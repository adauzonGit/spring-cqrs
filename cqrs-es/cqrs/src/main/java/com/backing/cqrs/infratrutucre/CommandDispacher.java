package com.backing.cqrs.infratrutucre;

import com.backing.cqrs.commands.BaseCommand;
import com.backing.cqrs.commands.CommandHandlerMethod;

public interface CommandDispacher {

    <T extends BaseCommand> void  registerHandler(Class<T> type , CommandHandlerMethod<T> handler);

    void send(BaseCommand command);

}
