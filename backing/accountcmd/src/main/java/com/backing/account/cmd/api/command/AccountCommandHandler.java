package com.backing.account.cmd.api.command;


import com.backing.account.cmd.domain.AccountAggregate;
import com.backing.cqrs.handlers.EvenSourcingHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountCommandHandler implements  CommandHandler{


    @Autowired
    private EvenSourcingHandler<AccountAggregate> evenSourcingHandler;

    @Override
    public void handle(OpenAccountCommand openAccountCommand) {

        var aggregate = new AccountAggregate(openAccountCommand);
        evenSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(DepositAccountCommand depositAccountCommand) {
        var aggregate = evenSourcingHandler.getById(depositAccountCommand.getId());
        aggregate.depositFunds(depositAccountCommand.getAmmount());
        evenSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(WithdrawFundsCommand withdrawFundsCommand) {

        var aggregate = evenSourcingHandler.getById(withdrawFundsCommand.getId());
        if(withdrawFundsCommand.getAmmount() > aggregate.getBalance()){
            throw  new IllegalStateException("No cuentas con fondos suficientes");
        }
        aggregate.withDrawFunds(withdrawFundsCommand.getAmmount());
        evenSourcingHandler.save(aggregate);

    }

    @Override
    public void handle(CloseAccountCommand closeAccountCommand) {

        var aggregate = evenSourcingHandler.getById(closeAccountCommand.getId());
        aggregate.closeAccount();
        evenSourcingHandler.save(aggregate);

    }
}
