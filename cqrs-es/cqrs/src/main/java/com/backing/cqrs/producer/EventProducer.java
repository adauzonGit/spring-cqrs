package com.backing.cqrs.producer;

import com.backing.cqrs.events.BaseEvent;

public interface EventProducer {

    void produce(String topic, BaseEvent baseEvent);

}
