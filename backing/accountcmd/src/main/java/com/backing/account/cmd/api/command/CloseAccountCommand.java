package com.backing.account.cmd.api.command;

import com.backing.cqrs.commands.BaseCommand;

public class CloseAccountCommand extends BaseCommand {

    public CloseAccountCommand(String id){
        super(id);
    }
}
