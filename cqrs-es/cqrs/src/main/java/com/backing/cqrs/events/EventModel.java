package com.backing.cqrs.events;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@Document("eventStore")
public class EventModel {

    @Id
    private String id;

    private Date timeStamp;

    private String aggregateIdentifier;

    private String aggrateType;

    private int version;

    private  String eventType;

    private  BaseEvent baseEvent;


}
