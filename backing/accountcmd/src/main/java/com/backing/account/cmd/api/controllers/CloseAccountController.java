package com.backing.account.cmd.api.controllers;

import com.backing.account.cmd.api.command.CloseAccountCommand;
import com.backing.account.cmd.api.command.DepositAccountCommand;
import com.backing.account.common.dto.BaseResponse;
import com.backing.cqrs.exceptions.AggregateNotFoundException;
import com.backing.cqrs.infratrutucre.CommandDispacher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/depositFunds")
public class CloseAccountController {
    
    
    private static final Logger log = LoggerFactory.getLogger(CloseAccountController.class);

    @Autowired
    private CommandDispacher commandDispacher;

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse>  closeAccount(@PathVariable(value = "id") String id ){

        try{

            var closeAccountCommand = new CloseAccountCommand(id);
            commandDispacher.send(closeAccountCommand);
            return ResponseEntity.status(200).body(new BaseResponse("Operacion exitosa"));

        }catch (IllegalStateException| AggregateNotFoundException ex){
            log.warn("Error al cerrar el request  ");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponse(ex.toString()));
        } catch (Exception ex){
            log.error("Error al cerrar el request  " + id);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new BaseResponse(ex.toString()));
        }

    }
    
}
