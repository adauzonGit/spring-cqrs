package com.backing.account.query.infrastructure.handlers;

import com.backing.account.common.events.AccountClosedEvent;
import com.backing.account.common.events.AccountOpenedEvent;
import com.backing.account.common.events.FundWithDrawEvent;
import com.backing.account.common.events.FundsDepositedEvent;
import com.backing.account.query.domain.AccountRepository;
import com.backing.account.query.domain.BanckAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountEventHandler implements EventHandler{

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public void on(AccountOpenedEvent accountOpenedEvent) {

        var bankAccount = BanckAccount.builder()
                .accountHolder(accountOpenedEvent.getAccountHolder())
                .creationDate(accountOpenedEvent.getCreatedDate())
                .accountType(accountOpenedEvent.getAccountType())
                .balance(accountOpenedEvent.getOpeningBalance())
                .id(accountOpenedEvent.getId())
                .build();
        accountRepository.save(bankAccount);
    }

    @Override
        public void on(AccountClosedEvent accountClosedEvent) {
        accountRepository.deleteById(accountClosedEvent.getId());
    }

    @Override
    public void on(FundsDepositedEvent fundsDepositedEvent) {

        var bankAccount = accountRepository.findById(fundsDepositedEvent.getId());
        if(!bankAccount.isPresent()){
            return;
        }

        var currentBalance = bankAccount.get().getBalance();

        var latestBalance = currentBalance + fundsDepositedEvent.getAmmount();

        bankAccount.get().setBalance(latestBalance);

        accountRepository.save(bankAccount.get());

    }

    @Override
    public void on(FundWithDrawEvent fundWithDrawEvent) {
        var bankAccount = accountRepository.findById(fundWithDrawEvent.getId());
        if(!bankAccount.isPresent()){
            return;
        }

        BanckAccount banckAccount = bankAccount.get();

        var latestBalance = banckAccount.getBalance() - fundWithDrawEvent.getAmmount();


        if(latestBalance < 0){
            return;
        }

        banckAccount.setBalance(latestBalance);
        accountRepository.save(banckAccount);



    }
}
