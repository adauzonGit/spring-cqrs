package com.backing.account.query.infrastructure.handlers;

import com.backing.account.common.events.AccountClosedEvent;
import com.backing.account.common.events.AccountOpenedEvent;
import com.backing.account.common.events.FundWithDrawEvent;
import com.backing.account.common.events.FundsDepositedEvent;

public interface EventHandler {
    void on(AccountOpenedEvent accountOpenedEvent);
    void on(AccountClosedEvent accountClosedEvent);
    void on(FundsDepositedEvent fundsDepositedEvent);
    void  on(FundWithDrawEvent fundWithDrawEvent);
}
