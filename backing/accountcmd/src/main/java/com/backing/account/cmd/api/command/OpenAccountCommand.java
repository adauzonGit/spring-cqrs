package com.backing.account.cmd.api.command;

import com.backing.account.common.dto.AccountType;
import com.backing.cqrs.commands.BaseCommand;
import lombok.Data;

@Data
public class OpenAccountCommand extends BaseCommand {

    private String accountHolder;

    private AccountType accountType;

    private double openingBalance;





}
